import os
import pytest
from pathlib import Path
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


@pytest.fixture()
def RoleSysVars(host):
    path = Path(os.environ["MOLECULE_PROJECT_DIRECTORY"], "vars")

    facts = host.ansible("setup")["ansible_facts"]

    for sys in ["ansible_distribution", "ansible_os_family"]:
        sys_file = path.joinpath(f"sys_{facts[sys]}.yml")
        if sys_file.exists():
            inc = host.ansible("include_vars", f"file={sys_file}")
            return inc["ansible_facts"]

    raise Exception("Cannot load role system variables")


def test_service(host, RoleSysVars):
    srv = host.service(RoleSysVars["chrony_service"])
    assert srv.is_running
    assert srv.is_enabled


def test_client(host):
    ntp_sources_file = "/tmp/test_ntp_sources"
    host.run_expect([0], f"chronyc sources >{ntp_sources_file}")
    assert host.file(ntp_sources_file).contains("spritz.osci.io")
    assert host.file(ntp_sources_file).contains("badcat.osci.io")
