# Ansible role for Chrony installation

## Introduction

[Chrony](https://chrony.tuxfamily.org/) is a NTP client implementation,
running as a daemon.

This role installs and configure the server. The server is configured to serve the local machine only.

## Usage

To read parameters documentation, use this command:

```
ansible-doc -t role chrony

```
